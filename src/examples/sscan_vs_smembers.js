const client = require("redis").createClient({
    port: 6379,
    host: '127.0.0.1',
    // password: 'password',
});
const getMembers1 = async key => client.smembersAsync(key);

// always follow getMembers2 in productions systems.


const getMembers2 = async (key) => {
    const members = [];
    let cursorPosition = '0';

    do {
        let scanResponse = await client.sscanAsync(
            key,
            cursorPosition,
            'MATCH',
            '*',
            'COUNT',
            1000
        );
        cursorPosition = scanResponse[0];
        members.push(...scanResponse[1]);
    } while (cursorPosition !== '0');

    return members;
};

// rate limiter using list

const hit = async (userId, maxHits) => {
    const d = new Date();
    const secondOfDay = d.getSeconds() + (60 * d.getUTCMinutes()) + (60 * 60 * d.getUTCHours());
    const key = `limiter:${secondOfDay}:${userId}`;
      
    const pipeline = client.batch();
    pipeline.lpush(key, Date.now());
    
    // pipeline.lrange(key, 0, -1);
    pipeline.expire(key, 1);
      
    const results = await pipeline.execAsync();
      
    if (results[0] > maxHits) {
      return 0;
    } else {
      return maxHits - results[0];
    }
  };

/*

Explanation
The call to pipeline.execAsync is in the right place,
as it occurs before we try to use any returned values from our pipelined commands.
Also, in this implementation, it doesn't matter whether we use pipeline.lpush or pipeline.rpush,
since we're merely counting the total number of entries in the list as the number of hits.
However, it's not necessary to call pipeline.lrange,
since pipeline.lpush will return the number of items in the list.
And it's possible that the key might not exist by the time pipeline.lrange is called.
This is because we've set the key to expire in one second,
and it's entirely possible that a long-running Redis command could be executed between the call to pipeline.
expire and the call to pipeline.lrange.

*/